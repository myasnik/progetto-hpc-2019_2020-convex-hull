# Progetto HPC 2019_2020 - Convex Hull

## Introduzione

Questo archivio contiene il materiale per il progetto del corso di
High Performance Computing, laurea in Ingegneria e Scienze
Informatiche, Universita' di Bologna sede di Cesena, AA 2019/2020.

Il file README (questo file) dovra' includere le istruzioni per la
compilazione e l'esecuzione dei programmi consegnati; per comodita',
nella directory src/ e' presente un Makefile che dovrebbe gia' essere
in grado di compilare le versioni OpenMP, MPI e/o CUDA eventualmente
presenti nella directory stessa. Si puo' modificare il Makefile
fornito, oppure si puo' decidere di non usarlo ed effettuare la
compilazione in modo manuale. In tal caso specificare in questo file i
comandi da usare per la compilazione dei programmi.

## Istruzioni per la compilazione

Utilizzare il Makefile, le istruzioni e i comandi disponibili sono al suo
interno.

## Note

- Sono stati scritti tre script di utility in Python per velocizzare 
la raccolta ed elaborazione dei dati, si trovano nella cartella src
con gli altri sorgenti:
    - eval.py: elaborazione dati e creazione dei grafici
    - run.py: esecuzione e raccolta risultati
    - run_weak.py: esecuzione e raccolta risultati relativi al weak scaling
- Questi script non sono commentati/ottimizzati in quanto non sono stati
pensati per la pubblicazione ma semplicemente come aiuto per la stesura
della relazione.
- Sono stati inoltre inseriti nella cartella log i risultati dei test
effettuati al fine di scrivere la relazione, analizzabili mediante lo
script eval.py.
