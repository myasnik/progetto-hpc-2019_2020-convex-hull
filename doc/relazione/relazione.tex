\documentclass[a4paper,12pt]{article}

\usepackage[bookmarks]{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{float}
\usepackage{minted}
\usepackage{tikz}
\usepackage{multirow}
\usepackage[nottoc]{tocbibind}
\usepackage{bookmark}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage[ddmmyyyy]{datetime}
\usepackage[italian]{cleveref}
\usepackage[italian]{babel}
\usepackage{titling}
\setlength{\droptitle}{-5em}
\usepackage{geometry}
\geometry{
 a4paper,
 total={170mm,257mm},
 left=18mm,
 right=18mm,
 top=20mm,
 }

\title{Progetto di High Performance Computing 2019/2020}
\author{Pietro Mazzini, matr. 0000838827}
\date{\today}

\begin{document}
 
\maketitle

\section{Introduzione}

Il progetto è stato svolto in maniera incrementale partendo dalla rielaborazione della versione seriale, nel tentativo di renderla più prestante. Questa fase si è rilevata utile per la comprensione dell'algoritmo di base ma non per il suo miglioramento.

Successivamente si è passati allo sviluppo della versione \texttt{OpenMP}. La ricerca di una soluzione a questa parte del progetto ha consentito di sviluppare idee utili anche per la fase di implementazione della versione \texttt{CUDA}. 

\section{Versione OpenMP}

\subsection{Parallelizzazione e funzionamento}

La versione \texttt{OpenMP} è stata sviluppata parallelizzando il ciclo \texttt{for} principale della funzione \newline \texttt{convex-hull()}:

\begin{minted}{c}
for (j=0; j<n; j++) {
    if (LEFT == turn(p[cur], p[next], p[j])) {
        next = j;
    }
}
\end{minted}

L'insieme delle iterazioni del ciclo è suddiviso fra i \texttt{thread\_count} threads a disposizione, in modo da ottenere come risultato un vettore di interi \texttt{maybe\_next[]} di dimensione \texttt{thread\_count}. Questo conterrà gli indici dei migliori punti, selezionati mediante la funzione \texttt{turn()} dai vari threads.

In seguito il thread principale scorrerà il vettore \texttt{maybe\_next[]}. L'applicazione della funzione \texttt{cw\_angle()} consente di attribuire ad ogni elemento del vettore un valore numerico. La scelta del minore di questi identificherà l'indice del punto corrispettivo; quest'ultimo rappresenterà il punto successivo dell'insieme calcolato.

Si sarebbe potuto eliminare il ciclo seriale implementando una specifica riduzione dei valori del \texttt{for} principale. Ciò non è stato eseguito poiché avrebbe complicato fortemente il codice senza garantire guadagni significativi a livello di prestazioni.

Per lo stesso motivo è stata tralasciata anche la parallelizzazione del ciclo incaricato di trovare il punto \texttt{p[leftmost]} del dominio.

\subsection{Prestazioni}

\textbf{Hardware utilizzato}: Intel(R) Xeon(R) CPU E5-2603 v4 @ 1.70GHz

Analizziamo la Figura \ref{fig1}.

Il grafico \ref{fig:omptem} illustra il tempo impiegato per risolvere le istanze del problema al variare del numero di threads assegnati.

Il grafico \ref{fig:ompsp} relativo allo \texttt{Speedup}, mostra un miglioramento delle prestazioni fino all'esecuzione con 11 threads per il caso peggiore, fino a 6 per quello intermedio ed un graduale e continuo peggioramento per il caso migliore.

Considerando il grafico \ref{fig:ompss} che rappresenta lo \texttt{Strong Scaling}, si può affermare che l'efficienza è direttamente proporzionale alla quantità dei punti, come deducibile dalla \texttt{Legge di Amdahl}. Ciò si verifica poiché il tempo utilizzato per eseguire la computazione parallela, nelle istanze più grandi del problema, è maggiore rispetto a quello impiegato per eseguire quella seriale.

Un comportamento analogo si può osservare nel grafico \ref{fig:ompws}, dedicato al \texttt{Weak Scaling}.

\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.49\linewidth}
  \centering
  \includegraphics[scale=0.39]{./img/omp_tem.png}
  \caption{Tempistiche di esecuzione}\label{fig:omptem}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\linewidth}
  \centering
  \includegraphics[scale=0.39]{./img/omp_sp.png}
  \caption{Speedup}\label{fig:ompsp}
  \end{subfigure}
  \centering
  \begin{subfigure}[b]{0.49\linewidth}
  \centering
  \includegraphics[scale=0.39]{./img/omp_ss.png}
  \caption{Strong Scaling}\label{fig:ompss}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\linewidth}
  \centering
  \includegraphics[scale=0.39]{./img/omp_ws.png}
  \caption{Weak Scaling}\label{fig:ompws}
  \end{subfigure}
  \caption{Grafici relativi alle prestazioni della versione \texttt{OpenMP}}\label{fig1}
\end{figure}

\section{Versione CUDA}

\subsection{Parallelizzazione e funzionamento}

In ambito \texttt{CUDA} il paradigma utilizzato è simile a quello adottato per \texttt{OpenMP}. La differenza principale è l'utilizzo di \texttt{cw\_angle()} al posto di \texttt{turn()}. Tramite il \texttt{kernel} \texttt{find\_best\_angle()} viene eseguita una riduzione sugli elementi del dominio di partenza, adottando la modalità illustrata durante le lezioni. Questa operazione selezionerà gli \texttt{n\_of\_blocks} punti migliori mediante la funzione \texttt{cw\_angle()}. Di seguito i punti verranno memorizzati all'interno di un vettore di strutture \texttt{point\_angle\_t}, ognuna delle quali contenente l'indice del punto selezionato e la misura dell'angolo relativo.

\begin{minted}{c}
typedef struct {
    int i;
    double angle;
} point_angle_t;
\end{minted}

L'analisi seriale del vettore di strutture consente di trovare il punto corrispondente al valore minimo dell'angolo. Questo sarà il punto successivo della soluzione.

Per risolvere il problema relativo all'utilizzo della funzione \texttt{cw\_angle()} solleveato dal professore nel file delle specifiche, prima della computazione parallela, è stato necessario implementare un ulteriore ciclo seriale per identificare il primo punto, successivo a \texttt{p[leftmost]}. 

L'applicazione di test di parallelizzazione sull'analisi seriale del vettore di strutture, il calcolo di \texttt{p[leftmost]} e il primo ciclo ha mostrato un peggioramento delle prestazioni. Ne è derivata la scelta di mantenere seriali le suddette funzioni. Infatti si può considerare inutile parallelizzare porzioni di codice che non hanno un impatto significativo sulle prestazioni.

\subsection{Prestazioni}

\textbf{Hardware utilizzato}: GPU GeForce GTX 1070

Analizziamo la Tabella \ref{tab:cuda}.

Si può notare che per le istanze più semplici del problema i risultati della versione \texttt{CUDA} non sono paragonabili a quelli della versione seriale.

Considerando invece le istanze di complessità maggiore, le prestazioni migliorano per la \texttt{Legge di Amdahl}. 

\begin{table}[ht]
\centering
\begin{tabular}{ccccc}
\toprule
 Input & T\textsubscript{seriale} (\textit{s}) &
 T\textsubscript{CUDA} (\textit{s}) & Speedup
 & Throughput (\textit{pts/s}) \\
 \midrule
    ace & 0.291604 & 0.316963 & 0.919994 & 1531601.480299 \\
    box1k & 0.000147 & 0.223809 & 0.000656 & 4468.099556 \\
    box10k & 0.001147 & 0.234647 & 0.004889 & 42617.244301 \\
    box100k & 0.013158 & 0.248725 & 0.052900 & 402050.457332 \\
    box1M & 0.140826 & 0.359343 & 0.391900 & 2782856.490874 \\
    circ1k & 0.007177 & 0.260903 & 0.027508 & 3832.845029 \\
    circ10k & 0.434279 & 0.474815 & 0.914626 & 21060.825348 \\
    circ100k & 41.487492 & 7.700512 & 5.387628 & 12986.149155 \\
    gaus100k & 0.007341 & 0.241335 & 0.030417 & 414361.092489 \\
    gaus1M & 0.075392 & 0.338713 & 0.222583 & 2952355.477771 \\
\bottomrule
\end{tabular}
\caption{Comparazione delle prestazioni della versione \texttt{CUDA} con quelle della versione seriale}\label{tab:cuda}
\end{table}

\section{Conclusioni}

L'analisi dei dati relativi alla versione \texttt{OpenMP}, considerando tutte le istanze del problema, ci consente di affermare che il valore di thread ottimo varia da \texttt{4} a \texttt{6}. Si può ipotizzare che ciò sia favorito dal fatto che la macchina su cui i test sono stati eseguiti possiede due processori da sei \texttt{core} ognuno, alloggiati in due \texttt{socket} differenti. È possibile che questa disposizione spaziale determini una maggiore efficienza.

Per quanto riguarda la versione \texttt{CUDA} i dati mostrano che ha significato utilizzare questo paradigma solo se l'esecuzione della versione seriale su un'istanza \texttt{x} del problema si protrae per qualche secondo. Differentemente l'overhead dovuto alla preparazione dei dispositivi \texttt{CUDA} diventerebbe il punto critico del programma.

\end{document}
