/**************************************************************************
 *
 * cuda-convex-hull.c
 *
 * Compute the convex hull of a set of points in 2D using OpenMP
 *
 * Pietro Mazzini 0000838827 <pietro.mazzini(at)studio.unibo.it>
 * Based on convex-hull.c by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************
 *
 * Questo programma calcola l'inviluppo convesso (convex hull) di un
 * insieme di punti 2D letti da standard input usando l'algoritmo
 * "gift wrapping" e CUDA. Le coordinate dei vertici dell'inviluppo sono
 * stampate su standard output.  Per una descrizione completa del
 * problema si veda la specifica del progetto sul sito del corso:
 *
 * http://moreno.marzolla.name/teaching/HPC/
 *
 * Per compilare: usare il MakeFile
 *
 * Per eseguire il programma utilizzare il MakeFile
 * 
 * Per visualizzare graficamente i punti e l'inviluppo calcolato è
 * possibile usare lo script di gnuplot (http://www.gnuplot.info/)
 * incluso nella specifica del progetto:
 *
 * gnuplot -c plot-hull.gp ace.in ace.hull ace.png
 *
 *************************************************************************/
#include "hpc.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <float.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef BLKDIM
/* Must be a power of two */
#define BLKDIM 1024
#endif

/* A single point */
typedef struct {
    double x, y;
} point_t;

/* Point id and angle */
typedef struct {
    int i;
    double angle;
} point_angle_t;

/* An array of n points */
typedef struct {
    int n;      /* number of points     */
    point_t *p; /* array of points      */
} points_t;

enum {
    LEFT = -1,
    COLLINEAR,
    RIGHT
};

/**
 * Get the clockwise angle between the line p0p1 and the vector p1p2 
 *
 *         .
 *        . 
 *       .--+ (this angle) 
 *      .   |    
 *     .    V
 *    p1--------------p2
 *    /
 *   /
 *  /
 * p0
 *
 */
__device__ __host__ double cw_angle(point_t p0, point_t p1, point_t p2)
{
    const double x1 = p2.x - p1.x;
    const double y1 = p2.y - p1.y;    
    const double x2 = p1.x - p0.x;
    const double y2 = p1.y - p0.y;
    const double dot = x1*x2 + y1*y2;
    const double det = x1*y2 - y1*x2;
    const double result = atan2(det, dot);
    return (result >= 0 ? result : 2*M_PI + result);
}


__global__ void find_best_angle( const point_t *p, int n, int cur,
        int next, point_angle_t *result )
{
    __shared__ point_angle_t local_best_angles[BLKDIM];
    int lindex = threadIdx.x,
        gindex = threadIdx.x + blockIdx.x * blockDim.x,
        bsize = blockDim.x / 2;
    
    /* Search for the next vertex */
    local_best_angles[lindex].i = -1;
    local_best_angles[lindex].angle = DBL_MAX;
    if ( gindex < n && gindex != next ) {
        local_best_angles[lindex].i = gindex;
        local_best_angles[lindex].angle = cw_angle(p[cur], p[next],
                p[gindex]);
    }

    /* Wait for all threads to finish the copy and calc operation */
    __syncthreads(); 

    /* All threads within the block cooperate to compute the local best */
    while ( bsize > 0 ) {
        if ( lindex < bsize && local_best_angles[lindex].angle >
                local_best_angles[lindex + bsize].angle ) {
            local_best_angles[lindex].i = local_best_angles[lindex +
                bsize].i;
            local_best_angles[lindex].angle = local_best_angles[lindex +
                bsize].angle;
        }
        bsize = bsize / 2; 
        /* Threads must synchronize before performing the next
           reduction step */
        __syncthreads(); 
    }

    if ( 0 == lindex ) {
        result[blockIdx.x].i = local_best_angles[lindex].i;
        result[blockIdx.x].angle = local_best_angles[lindex].angle;
    }
}


/**
 * Read input from file f, and store the set of points into the
 * structure pset.
 */
void read_input( FILE *f, points_t *pset )
{
    char buf[1024];
    int i, dim, npoints;
    point_t *points;
    
    if ( 1 != fscanf(f, "%d", &dim) ) {
        fprintf(stderr, "FATAL: can not read dimension\n");
        exit(EXIT_FAILURE);
    }
    if (dim != 2) {
        fprintf(stderr, "FATAL: This program supports dimension 2 only"
                " (got dimension %d instead)\n", dim);
        exit(EXIT_FAILURE);
    }
    if (NULL == fgets(buf, sizeof(buf), f)) { /* ignore rest of the line */
        fprintf(stderr, "FATAL: failed to read rest of first line\n");
        exit(EXIT_FAILURE);
    }
    if (1 != fscanf(f, "%d", &npoints)) {
        fprintf(stderr, "FATAL: can not read number of points\n");
        exit(EXIT_FAILURE);
    }
    assert(npoints > 2);
    points = (point_t*)malloc( npoints * sizeof(*points) );
    assert(points);
    for (i=0; i<npoints; i++) {
        if (2 != fscanf(f, "%lf %lf", &(points[i].x), &(points[i].y))) {
            fprintf(stderr, "FATAL: failed to get"
                    " coordinates of point %d\n", i);
            exit(EXIT_FAILURE);
        }
    }
    pset->n = npoints;
    pset->p = points;
}

/**
 * Free the memory allocated by structure pset.
 */
void free_pointset( points_t *pset )
{
    pset->n = 0;
    free(pset->p);
    pset->p = NULL;
}

/**
 * Dump the convex hull to file f. The first line is the number of
 * dimensione (always 2); the second line is the number of vertices of
 * the hull PLUS ONE; the next (n+1) lines are the vertices of the
 * hull, in clockwise order. The first point is repeated twice, in
 * order to be able to plot the result using gnuplot as a closed
 * polygon
 */
void write_hull( FILE *f, const points_t *hull )
{
    int i;
    fprintf(f, "%d\n%d\n", 2, hull->n + 1);
    for (i=0; i<hull->n; i++) {
        fprintf(f, "%f %f\n", hull->p[i].x, hull->p[i].y);
    }
    /* write again the coordinates of the first point */
    fprintf(f, "%f %f\n", hull->p[0].x, hull->p[0].y);    
}

/**
 * Compute the convex hull of all points in pset using the "Gift
 * Wrapping" algorithm. The vertices are stored in the hull data
 * structure, that does not need to be initialized by the caller.
 */
void convex_hull(const points_t *pset, points_t *hull)
{
    const int n = pset->n;
    point_t *d_p, *p = pset->p, fake_point;
    point_angle_t *d_result, *result;
    const int n_of_blocks = (n + BLKDIM - 1)/BLKDIM;
    const size_t size = n * sizeof(point_t), size_result =
        n_of_blocks * sizeof(point_angle_t);
    int i, j, cur, next, leftmost, best;
    double angle, cur_angle;

    hull->n = 0;
    /* There can be at most n points in the convex hull. At the end of
       this function we trim the excess space. */
    hull->p = (point_t*)malloc(n * sizeof(*(hull->p))); assert(hull->p);
    
    /* Identify the leftmost point p[leftmost] */
    leftmost = 0;
    for (i = 1; i<n; i++) {
        if (p[i].x < p[leftmost].x) {
            leftmost = i;
        }
    }
    cur = leftmost; 

    /* Malloc memory for host result */
    result = (point_angle_t*)malloc(size_result); assert(result);
    /* Check if BLKDIM is a power of two using the "bit hack" */
    assert( (BLKDIM & (BLKDIM-1)) == 0 );
    /* Cuda mallocs */
    cudaMalloc((void **)&d_p, size);
    cudaMalloc((void **)&d_result, size_result);
    /* Copy inputs to device */
    cudaMemcpy(d_p, p, size, cudaMemcpyHostToDevice);

    /* Main loop of the Gift Wrapping algorithm. This is where most of
       the time is spent; therefore, this is the block of code that
       must be parallelized. */
    /* First cycle */
    angle = DBL_MAX;
    fake_point.x = p[cur].x;
    fake_point.y = p[cur].y - 1;
    for (j=0; j<n; j++) {
        cur_angle = cw_angle(fake_point, p[cur], p[j]);
        if (cur_angle < angle && j != cur) {
            next = j;
            angle = cur_angle;
        }
    }
    do {
        /* Add the current vertex to the hull */
        assert(hull->n < n);
        hull->p[hull->n] = p[cur];
        hull->n++;
        
        /* Launch find_best_angle() kernel on the GPU */
        find_best_angle<<<n_of_blocks, BLKDIM>>>(d_p, n, cur, next,
                d_result);

        /* Copy the result from device memory to host memory */
        cudaMemcpy(result, d_result, size_result, cudaMemcpyDeviceToHost);

        /* Find best point of n_of_blocks returned by find_best_angle */
        angle = DBL_MAX;
        for (j=0; j<n_of_blocks; j++) {
            if (result[j].angle < angle) {
                best = result[j].i;
                angle = result[j].angle;
            } 
        }
        assert(cur != next);
        cur = next;
        next = best;
    } while (cur != leftmost);
    
    /* Trim the excess space in the convex hull array */
    hull->p = (point_t*)realloc(hull->p, (hull->n) * sizeof(*(hull->p)));
    assert(hull->p); 

    /* Cleanup */
    cudaFree(d_p); cudaFree(d_result); free(result);
}

/**
 * Compute the area ("volume", in qconvex terminoloty) of a convex
 * polygon whose vertices are stored in pset using Gauss' area formula
 * (also known as the "shoelace formula"). See:
 *
 * https://en.wikipedia.org/wiki/Shoelace_formula
 *
 * This function does not need to be parallelized.
 */
double hull_volume( const points_t *hull )
{
    const int n = hull->n;
    const point_t *p = hull->p;
    double sum = 0.0;
    int i;
    for (i=0; i<n-1; i++) {
        sum += ( p[i].x * p[i+1].y - p[i+1].x * p[i].y );
    }
    sum += p[n-1].x*p[0].y - p[0].x*p[n-1].y;
    return 0.5*fabs(sum);
}

/**
 * Compute the length of the perimeter ("facet area", in qconvex
 * terminoloty) of a convex polygon whose vertices are stored in pset.
 * This function does not need to be parallelized.
 */
double hull_facet_area( const points_t *hull )
{
    const int n = hull->n;
    const point_t *p = hull->p;
    double length = 0.0;
    int i;
    for (i=0; i<n-1; i++) {
        length += hypot( p[i].x - p[i+1].x, p[i].y - p[i+1].y );
    }
    /* Add the n-th side connecting point n-1 to point 0 */
    length += hypot( p[n-1].x - p[0].x, p[n-1].y - p[0].y );
    return length;
}

int main( void )
{
    points_t pset, hull;
    double tstart, elapsed;
    
    read_input(stdin, &pset);
    tstart = hpc_gettime();
    convex_hull(&pset, &hull);
    elapsed = hpc_gettime() - tstart;
    fprintf(stderr, "\nConvex hull of %d points in 2-d:\n\n", pset.n);
    fprintf(stderr, "  Number of vertices: %d\n", hull.n);
    fprintf(stderr, "  Total facet area: %f\n", hull_facet_area(&hull));
    fprintf(stderr, "  Total volume: %f\n\n", hull_volume(&hull));
    fprintf(stderr, "Elapsed time: %f\n\n", elapsed);
    write_hull(stdout, &hull);
    free_pointset(&pset);
    free_pointset(&hull);
    return EXIT_SUCCESS;    
}

