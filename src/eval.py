#!/usr/bin/env python3

import ast
import csv
import matplotlib.pyplot as plt

# Read OMP times
res = {'ace': [], 'box1k': [], 'box10k': [], 'box100k': [], 'box1M': [],
        'circ1k': [], 'circ10k': [], 'circ100k': [], 'gaus100k': [],
        'gaus1M': []}
cores = 12
for i in range(1, cores + 1):
    with open('../logs/omp_th_' + str(i)) as f:
        data = ast.literal_eval(f.readlines()[-1])
    res['ace'].append(float(data['ace']))
    res['box1k'].append(float(data['box1k']))
    res['box10k'].append(float(data['box10k']))
    res['box100k'].append(float(data['box100k']))
    res['box1M'].append(float(data['box1M']))
    res['circ1k'].append(float(data['circ1k']))
    res['circ10k'].append(float(data['circ10k']))
    res['circ100k'].append(float(data['circ100k']))
    res['gaus100k'].append(float(data['gaus100k']))
    res['gaus1M'].append(float(data['gaus1M']))

# Read OMP times for Weak Scaling
with open('../logs/omp_ws') as f:
    res_ws = ast.literal_eval(f.readline())

# Read CUDA times
with open('../logs/cuda') as f:
    cuda_res = ast.literal_eval(f.readlines()[-1])

# OMP Speedup
os = {'ace': [], 'box1k': [], 'box10k': [], 'box100k': [], 'box1M': [],
        'circ1k': [], 'circ10k': [], 'circ100k': [], 'gaus100k': [],
        'gaus1M': []}
for i in range(cores):
    os['ace'].append(res['ace'][0]/res['ace'][i])
    os['box1k'].append(res['box1k'][0]/res['box1k'][i])
    os['box10k'].append(res['box10k'][0]/res['box10k'][i])
    os['box100k'].append(res['box100k'][0]/res['box100k'][i])
    os['box1M'].append(res['box1M'][0]/res['box1M'][i])
    os['circ1k'].append(res['circ1k'][0]/res['circ1k'][i])
    os['circ10k'].append(res['circ10k'][0]/res['circ10k'][i])
    os['circ100k'].append(res['circ100k'][0]/res['circ100k'][i])
    os['gaus100k'].append(res['gaus100k'][0]/res['gaus100k'][i])
    os['gaus1M'].append(res['gaus1M'][0]/res['gaus1M'][i])

# OMP Strong scaling
oss = {'ace': [], 'box1k': [], 'box10k': [], 'box100k': [], 'box1M': [],
        'circ1k': [], 'circ10k': [], 'circ100k': [], 'gaus100k': [],
        'gaus1M': []}
for i in range(cores):
    oss['ace'].append(os['ace'][i]/(i+1))
    oss['box1k'].append(os['box1k'][i]/(i+1))
    oss['box10k'].append(os['box10k'][i]/(i+1))
    oss['box100k'].append(os['box100k'][i]/(i+1))
    oss['box1M'].append(os['box1M'][i]/(i+1))
    oss['circ1k'].append(os['circ1k'][i]/(i+1))
    oss['circ10k'].append(os['circ10k'][i]/(i+1))
    oss['circ100k'].append(os['circ100k'][i]/(i+1))
    oss['gaus100k'].append(os['gaus100k'][i]/(i+1))
    oss['gaus1M'].append(os['gaus1M'][i]/(i+1))

# OMP Weak Scaling
ows = {'circ1k': [], 'circ10k': [], 'circ100k': []} 
for i in range(cores):
    ows['circ1k'].append(res_ws['1000'][0]/res_ws['1000'][i])
    ows['circ10k'].append(res_ws['10000'][0]/res_ws['10000'][i])
    ows['circ100k'].append(res_ws['100000'][0]/res_ws['100000'][i])

# CUDA Times 
print('## CUDA Times ##')
print(cuda_res)

# CUDA Speedup
csp = {'ace': res['ace'][0]/cuda_res['ace'],
       'box1k': res['box1k'][0]/cuda_res['box1k'],
       'box10k': res['box10k'][0]/cuda_res['box10k'],
       'box100k': res['box100k'][0]/cuda_res['box100k'],
       'box1M': res['box1M'][0]/cuda_res['box1M'],
       'circ1k': res['circ1k'][0]/cuda_res['circ1k'],
       'circ10k': res['circ10k'][0]/cuda_res['circ10k'],
       'circ100k': res['circ100k'][0]/cuda_res['circ100k'],
       'gaus100k': res['gaus100k'][0]/cuda_res['gaus100k'],
       'gaus1M': res['gaus1M'][0]/cuda_res['gaus1M']}
print('\n## CUDA Speedup ##')
print(csp)

# CUDA Throughput
cth = {'ace': 485461/cuda_res['ace'],
       'box1k': 1000/cuda_res['box1k'],
       'box10k': 10000/cuda_res['box10k'],
       'box100k': 100000/cuda_res['box100k'],
       'box1M': 1000000/cuda_res['box1M'],
       'circ1k': 1000/cuda_res['circ1k'],
       'circ10k': 10000/cuda_res['circ10k'],
       'circ100k': 100000/cuda_res['circ100k'],
       'gaus100k': 100000/cuda_res['gaus100k'],
       'gaus1M': 1000000/cuda_res['gaus1M']}
print('\n## CUDA Throughput ##')
print(cth)

# Graphs
x = list(range(1, cores + 1)) 
# OMP Times
#Circ
plt.plot(x, res['circ1k'], label = "Circ1k")
plt.plot(x, res['circ10k'], label = "Circ10k") 
plt.plot(x, res['circ100k'], label = "Circ100k")
plt.title('Tempistiche')
plt.xlabel('# Threads')
plt.ylabel('Secondi')
plt.legend()
plt.grid()
plt.show()
# Box
plt.plot(x, res['box1k'], label = "Box1k")
plt.plot(x, res['box10k'], label = "Box10k") 
plt.plot(x, res['box100k'], label = "Box100k")
plt.plot(x, res['box1M'], label = "Box1M")
plt.title('Tempistiche')
plt.xlabel('# Threads')
plt.ylabel('Secondi')
plt.legend()
plt.grid()
plt.show()
# Ace
plt.plot(x, res['ace'], label = "ace")
plt.title('Tempistiche')
plt.xlabel('# Threads')
plt.ylabel('Secondi')
plt.legend()
plt.grid()
plt.show()
# Gaus
plt.plot(x, res['gaus100k'], label = "Gaus100k")
plt.plot(x, res['gaus1M'], label = "Gaus1M")
plt.title('Tempistiche')
plt.xlabel('# Threads')
plt.ylabel('Secondi')
plt.legend()
plt.grid()
plt.show()
# OMP Speedup
# Circ
plt.plot(x, os['circ1k'], label = "Circ1k")
plt.plot(x, os['circ10k'], label = "Circ10k") 
plt.plot(x, os['circ100k'], label = "Circ100k")
plt.title('Speedup')
plt.xlabel('# Threads')
plt.ylabel('Speedup')
plt.legend()
plt.grid()
plt.show()
# Box
plt.plot(x, os['box1k'], label = "Box1k")
plt.plot(x, os['box10k'], label = "Box10k") 
plt.plot(x, os['box100k'], label = "Box100k")
plt.plot(x, os['box1M'], label = "Box1M")
plt.title('Speedup')
plt.xlabel('# Threads')
plt.ylabel('Speedup')
plt.legend()
plt.grid()
plt.show()
# Ace
plt.plot(x, os['ace'], label = "ace")
plt.title('Speedup')
plt.xlabel('# Threads')
plt.ylabel('Speedup')
plt.legend()
plt.grid()
plt.show()
# Gaus
plt.plot(x, os['gaus100k'], label = "Gaus100k")
plt.plot(x, os['gaus1M'], label = "Gaus1M")
plt.title('Speedup')
plt.xlabel('# Threads')
plt.ylabel('Speedup')
plt.legend()
plt.grid()
plt.show()
# OMP Strong scaling
# Circ
plt.plot(x, oss['circ1k'], label = "Circ1k")
plt.plot(x, oss['circ10k'], label = "Circ10k") 
plt.plot(x, oss['circ100k'], label = "Circ100k")
plt.title('Strong Scaling')
plt.xlabel('# Threads')
plt.ylabel('Efficienza')
plt.legend()
plt.grid()
plt.show()
# Box
plt.plot(x, oss['box1k'], label = "Box1k")
plt.plot(x, oss['box10k'], label = "Box10k") 
plt.plot(x, oss['box100k'], label = "Box100k")
plt.plot(x, oss['box1M'], label = "Box1M")
plt.title('Strong Scaling')
plt.xlabel('# Threads')
plt.ylabel('Efficienza')
plt.legend()
plt.grid()
plt.show()
# Ace
plt.plot(x, oss['ace'], label = "ace")
plt.title('Strong Scaling')
plt.xlabel('# Threads')
plt.ylabel('Efficienza')
plt.legend()
plt.grid()
plt.show()
# Gaus
plt.plot(x, oss['gaus100k'], label = "Gaus100k")
plt.plot(x, oss['gaus1M'], label = "Gaus1M")
plt.title('Strong Scaling')
plt.xlabel('# Threads')
plt.ylabel('Efficienza')
plt.legend()
plt.grid()
plt.show()
# OMP Weak scaling
# Circ
plt.plot(x, ows['circ1k'], label = "Circ1k")
plt.plot(x, ows['circ10k'], label = "Circ10k") 
plt.plot(x, ows['circ100k'], label = "Circ100k")
plt.title('Weak Scaling')
plt.xlabel('# Threads')
plt.ylabel('Efficienza')
plt.legend()
plt.grid()
plt.show()
