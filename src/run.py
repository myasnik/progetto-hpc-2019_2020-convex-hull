#!/usr/bin/env python3

import subprocess
import sys
import os
import re
import time

times = int(sys.argv[1])
command_img = 'make ' + sys.argv[2]
if sys.argv[2] == 'omp-images':
    os.putenv("OMP_NUM_THREADS", sys.argv[3])
command_del = 'make clean'
res = {'ace': [], 'box1k': [], 'box10k': [], 'box100k': [], 'box1M': [],
        'circ1k': [], 'circ10k': [], 'circ100k': [], 'gaus100k': [],
        'gaus1M': []}

subprocess.Popen(command_del.split(), stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
time.sleep(1)

for i in range(times):
    call = subprocess.Popen(command_img.split(), stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    output = call.stderr.read().decode("utf-8")
    result = re.findall('Elapsed time: (.*)\n', output)
    res['ace'].append(float(result[0]))
    res['box1k'].append(float(result[1]))
    res['box10k'].append(float(result[2]))
    res['box100k'].append(float(result[3]))
    res['box1M'].append(float(result[4]))
    res['circ1k'].append(float(result[5]))
    res['circ10k'].append(float(result[6]))
    res['circ100k'].append(float(result[7]))
    res['gaus100k'].append(float(result[8]))
    res['gaus1M'].append(float(result[9]))
    subprocess.Popen(command_del.split(), stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    time.sleep(1)

print('All results:')
print(res)

for k, v in res.items():
    v = sum(v) / len(v)
    res[k] = v

print('Average results:')
print(res)
