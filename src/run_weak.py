#!/usr/bin/env python3

import subprocess
import sys
import os
import re
import time

times = int(sys.argv[1])
max_threads = 4
res_circ = {'1000': [], '10000': [], '100000': []}
command_ex = './omp-convex-hull'

for k, v in res_circ.items():
    unit = int(int(k)/max_threads)
    for i in range(1, max_threads + 1):
        command_gen_points = 'rbox s ' + str(unit * i) + ' D2'
        in_proc = subprocess.Popen(command_gen_points.split(),
                stdout=subprocess.PIPE)
        in_file = in_proc.stdout.read()
        temp = []
        for _ in range(times):
            os.putenv("OMP_NUM_THREADS", str(i))
            call = subprocess.Popen(command_ex.split(),
                    stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE)
            stdout_data, stderr_data = call.communicate(input=in_file)
            result = re.findall('Elapsed time: (.*)\n', 
                    stderr_data.decode("utf-8"))
            temp.append(float(result[0]))
        res_circ[k].append(sum(temp)/len(temp))
print(res_circ)
